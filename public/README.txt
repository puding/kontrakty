=======
 SETUP
=======

Ako prvé zapni lokálny server:
    - Ak si na Windowse, dvojklikni na server.exe v tomto foldri, systém bude
      pravdepodobne frflať že možno je to vírus (nie je :D), takže vždy
      poklikaj Advanced a Run anyway alebo také čosi, keď to bude chcieť
      povolenie na network tak mu to dovoľ. Ak je všetko ok, otvorí sa ti malý
      čierny terminál - nechytaj ho a nevypínaj ho.
    - Ak si na Macu alebo Linuxe, !!!v tejto zložke!!! si otvor terminál
      a napíš príkaz:
            python -m SimpleHTTPServer
      Ak hodí errory, malo by namiesto toho fungovať:
            python -m http.server
      Výsledkom je opäť malý pustený terminál ktorý necháme bežať.

Toto treba preto, aby to celé vedelo bežať bez internetu.


============
 POUŽÍVANIE
============

Ovládanie hry otvoríš v browseri (preferovane Chrome alebo Firefox) na adrese
    http://localhost:8000/
Zobrazenie pre deti otvor na tom istom compe, v novom okne. Adresa:
    http://localhost:8000/view.html
Otváraj ich vždy v tomto poradí, každé len raz. Ak niektorá z ich nejde
otvoriť, vypni server (čierne okienko) a spusti ho znova.

Defaultne je nastavených 5 družiniek, na ovládacej stránke by malo byť
príslušných 5 prázdnych tabuliek s ich kontraktmi.

Kontrakt zadáš tak, že do vstupného riadka hore napíšeš práve tieto veci:
    - Rekurzívny predpis funkcie, čiže napríklad ak má kontrakt v každej
      iterácii pridať k skóre 10, funkcia je X+10, ak ho má znížiť na tretinu,
      funkcia je X/3. Okrem +, -, *, /, () a čísel sú dovolené všetky funkcie,
      ktoré nájdeš tu:
      https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math#Methods
      Ako premenné môžeš používať !veľké! X, čo je aktuálne skóre, a !veľké! T,
      čo je aktuálne poradové číslo iterácie (začína sa od 1).
      !!! POZOR !!! Vo funkcii nepoužívaj medzery (tie oddeľujú hodnoty
      popísané nižšie) a používaj desatinnú BODKU, čiarka (opäť bez medzery)
      oddeľuje argumenty vo funkciách, ktoré berú viac argumentov.
    - [medzera]
    - Počet sekúnd, ako dlho má trvať jedna iterácia (celé číslo, aspoň 1).
    - [medzera]
    - Počet iterácii, koľko má kontrakt trvať (celé číslo, aspoň 1). Môže byť
      aj Infinity, vtedy bude kontrakt platiť neobmedzene.
    - [voliteľne] [medzera]
    - [voliteľne] Názov kontraktu. Pri písaní kontraktu ti totiž vyskočí
      pomocné autocomplete okienko, kde sú nejaké preddefinované kontrakty a
      tiež všetky kontrakty, ktoré si doteraz zadal. Názov ti teda môže pomôcť
      pri ich hľadaní v budúcnosti.
Keď máš kontrakt zadaný, klikni na tlačidlo družinky, ktorej ho chceš pridať,
prípadne na VŠETCI, čo ho pridá všetkým (užitočné napríklad na reset hry, vtedy
stačí zadať '1000 1 1' a VŠETCI).

! POZOR ! Má to nakódené nejaké basic kontrolovanie platnosti vstupu, ale ak
tam napríklad napíšeš '2X 1 1 wannabedouble', tak pre skóre 1000 z toho
po jednej iterácii nespraví 2000, ale 21000, takže proste bacha na tie vstupy.

BTW, celé sa to dá ovládať klávesnicou, Tab a Shift+Tab prepína medzi vstupom
a jednotlivými tlačidlami, šípky hore a dole vyberajú z ponúkaných vstupov a
Enter potvrdzuje výber aj stláča tlačítko.

Po pridaní by sa mal kontrakt ukázať ako riadok v tabuľke príslušnej družinky,
pričom by mu v stĺpci secs mali pribúdať sekundy trvania aktuálnej iterácie
a v stĺpci iters je poradové číslo aktuálnej iterácie. Kontrakt zmizne sám keď
sa dokončí, alebo sa dá zastaviť a zmazať malým krížikom v tabuľke.

! POZOR ! Keď zavrieš stránku ovládania, posledné skóre sa ešte dá zistiť
(napr. na stránke zobrazenia pre deti), aktívne kontrakty však už nie. Keď
znovu otvoríš stránku ovládania, skóre sa zresetujú na štartovné hodnoty.

Na stránke zobrazenia nie je moc čo robiť (len to bachnuť na fullscreen na
projektor), mali by sa tam zrkadliť aktuálne skóre v reálnom čase.


============
 NASTAVENIA
============

Otvor si súbor config.js v textovom editore. Sú tam štyri hlavné veci, každú
z nich si môžeš poeditovať podľa svojej ľubovôle, vzor tam je. Komenty sa píšu
za // ako vo väčšine programovacích jazykov.

TEAMS je zoznam tímov, každý musí mať vlastnosti:
    - shortName: Unikátny názov, zobrazuje sa tebe v ovládaní, mal by mať
      tak okolo päť až sedem znakov, ak to má vyzerať pekne.
    - fullName: Zobrazuje sa deťom, celé pekné meno družinky, ale tiež by
      nemalo byť extra dlhé.
    - score: Začiatočné skóre.
    - color: Farba, ktorú má daný stĺpec v zobrazení pre deti.

My.[...] sú definície vlastných funkcii. Definuješ ich ako normálne
matematické funkcie so vstupom a výstupom (viď vzor v config.js), pričom
potom v hre aj v preddefinovaných kontraktoch ich vieš používať úplne rovnako
ako tie z Math (viď link v zadávaní kontraktov).

CONTRACTS je zoznam preddefinovaných kontraktov, ktoré si môžete spísať pred
hrou a potom sa budú inteligentne zobrazovať v autocomplete zozname. Tie, čo
pridáš ručne počas hry, sa budú počas tej hry zobrazovať v autocomplete, ale
sem sa nedopíšu. Formát je rovnaký ako pri zadávaní počas hry.

STYLE je zopár premenných, ktoré určujú hlavne spacing a veľkosti vecí
v zobrazení pre deti. Snažil som sa to spraviť tak, aby to vyzeralo rozumne
relatívne všade, ale ak si nastavíte správny počet a názvy tímov a otvoríte
to na projektore a niečo (z toho čo sa tu dá zmeniť - šírka stĺpcov, ...) by
nevyzeralo dobre, tak nato to tu je, aby sa to prestavilo.

GLHF!
