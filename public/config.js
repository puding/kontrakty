/* eslint-disable no-undef */

TEAMS = [
    {
        shortName: 'Prvý',
        fullName: 'Prvý Tím',
        score: 1500.00,
        color: 'rgb(177, 235, 0)'
    },
    {
        shortName: 'Druhý',
        fullName: 'Druhý Tím',
        score: 1000.00,
        color: 'rgb(83, 187, 244)'
    },
    {
        shortName: 'Jednorož',
        fullName: 'Jednorožce',
        score: 1200.00,
        color: 'rgb(255, 133, 203)'
    },
    {
        shortName: 'Tretí',
        fullName: 'Tretí tím dlhý názov',
        score: 1300.00,
        color: 'rgb(255, 67, 46)'
    },
    {
        shortName: 'Štvrtý',
        fullName: 'Štvrtý Tím',
        score: 1100.00,
        color: 'rgb(255, 172, 0)'
    },
]


My = {}
// My.nazovfunkcie = (parametre) => (hodnotafunkcie)
My.rand  = (min,max) => (min+Math.random()*(max-min))
My.round = (x,e) => (Math.pow(10,e)*Math.round(x/Math.pow(10,e)))
My.floor = (x,e) => (Math.pow(10,e)*Math.floor(x/Math.pow(10,e)))
My.ceil  = (x,e) => (Math.pow(10,e)*Math.ceil(x/Math.pow(10,e)))


CONTRACTS = [
    '2*X 60 3 double',       // trikrat zdvojnasobi, vzdy po minute
    'X+1 1 Infinity plus1',  // kazdu sekundu prida 1 donekonecna
    'X+(20-3*T) 10 5 yolo',  // v rozostupoch 10s prida 17, 14, 11, 8, 5
    '0 1 1 nuluj',           // jednorazovo vynuluje
    'X+10*Math.random() 10 Infinity', // kazdych 10s prida nahodne cislo od 0 do 10
    'My.ceil(X,2) 60 Infinity' // kazdu minutu sa skore zaokruhli nahor na stovky
]


STYLE = {
    barMaxH: 90,         // maximalna vyska stlpca (v % vysky obrazovky)
    barMinH: '3em',      // minimalna vyska stlpca (v jednotkach em)
    barW: 12,            // sirka stlpca (v % sirky obrazovky)
    barMar: 18,          // bocne okraje obrazovky (v % sirky obrazovky)
    nameTextZoom: 0.20,  // zvacsenie textu nazvu timu
    scoreTextZoom: 0.20, // zvacsenie textu score
}
