# Kontrakty

A game used once on a math camp for kids.

Every team starts with a certain amount of money and they can buy *contracts* during the game.
A contract is a mathematical function modifying their amount of money - eg. it can add $1 every
second for the remaining game time or double your money once in a minute three times. The goal of
each team is to maximize their amount of money on the game's end.

More info in [public/README.txt](public/README.txt) (Slovak language only).

Admin's interface for contract management:

![Admin's interface](images/app.png)

View for teams:

![View for teams](images/view.png)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```
