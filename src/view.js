import Vue from 'vue'
import View from './View.vue'

Vue.config.productionTip = false

new Vue({
  render: h => h(View),
}).$mount('#app')
