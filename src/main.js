import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'
import App from './App.vue'

Vue.use(Buefy)

Vue.config.productionTip = false

window.addEventListener('beforeunload', function (e) {
    e.preventDefault()
    e.returnValue = ''
});

new Vue({
  render: h => h(App),
}).$mount('#app')
